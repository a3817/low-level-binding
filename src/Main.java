
import javafx.application.Application;
import javafx.beans.binding.DoubleBinding;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.stage.Stage;



public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Group root = new Group();
        Scene scene = new Scene(root, 500, 400);
        stage.setScene(scene);
        stage.show();

        Slider slider = new Slider(0, 100, 0);
        root.getChildren().add(slider);
        slider.setLayoutX(100);
        slider.setLayoutY(100);
        TextField textField = new TextField();
        textField.setLayoutX(100);
        textField.setLayoutY(150);
        root.getChildren().add(textField);

        DoubleBinding db = new DoubleBinding() {
            {
                super.bind(slider.valueProperty());
            }
            @Override
            protected double computeValue() {
                return slider.getValue();
            }
        };
        textField.textProperty().bind(db.asString());

    }
}
